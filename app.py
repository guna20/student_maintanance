from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse


from Operations import *

app = FastAPI()
class Student(BaseModel):
    name : str
    dob : str
    clas : str
    section : str
    clsTeacherName: str
    email : str


@app.get('/author')
async def author():
    return "guna"

@app.get('/')
async def retrieve_all_students():
    student = await retrieve_students()
    return student


@app.get('/{id}/')
async def retrieve_one_student(id : str):
    student = await retrieve_student(id)
    return student

@app.put('/')
async def create_student(student:Student ):
    stu_json = jsonable_encoder(student)
    student = await add_student(stu_json)    
    return student


@app.delete("/{std_id}/")
async def del_a_student(std_id : str):
    student = await delete_student(std_id)
    if student:
        return student
    

@app.put("/{stu_id}/")
async def update_students(stu_id : str , data: dict) -> dict:
    student = await update_student(stu_id, data)
    if student == False:
        return "Student not updated"
    else:
        return student

